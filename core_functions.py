import nestpy
import scipy.stats as ss
from scipy.optimize import curve_fit
from matplotlib.colors import LogNorm
import numpy as np
#from math import erf
from scipy.special import erf

'''
The goal is to do as little repititon as possible. 
Since GetYields and GetQuanta do not depend on most detector parameters, we want to seperate the S1+S2 calculation from the yields calculation

Only repeat the yields/quanta calculations if the field changes

Only generate an energy spectrum once, and only generate a position spectrum once
'''
#Generate the quanta based on input energy and field only
def generate_quanta( NESTcalc, interaction, energy_array, field ):
	quantas = []
	for energy in energy_array:
		yields = NESTcalc.GetYields( interaction, energy, drift_field = field )
		quanta = NESTcalc.GetQuanta( yields )
		quantas.append( quanta )

	return np.array( quantas )


# Create a custom beta+gamma weighting for ER, based on execNEST.cpp 
def generate_quanta_ERweighted( NESTcalc, energy_array, field ):
	quantas = []
	yields = NESTcalc.GetYields( nestpy.INTERACTION_TYPE(8), 5., drift_field = field )

	#A, B, C, D, E, F = 0.5, 0.5, 1.1, -5., 1.01, 0.95 # LUX Run3
	A, B, C, D, E, F = 1., 0.55, -1.6, -1.0, 0.99, 1.04 # XENON10
	#A, B, C, D, E, F = -0.1, 0.5, 0.06, -0.6, 1.11, 0.95 # LUX C14
	for energy in energy_array:
		yieldsBeta = NESTcalc.GetYields( nestpy.INTERACTION_TYPE(8), energy, drift_field = field )
		yieldsGamma = NESTcalc.GetYields( nestpy.INTERACTION_TYPE(7), energy, drift_field = field )
		weightG = A + B * erf( C * (np.log( energy ) + D ) ) 	
		weightB = 1. - weightG

		yields.PhotonYield = weightG * yieldsGamma.PhotonYield + weightB * yieldsBeta.PhotonYield
		yields.ElectronYield = weightG * yieldsGamma.ElectronYield + weightB * yieldsBeta.ElectronYield
		yields.ExcitonRatio = weightG * yieldsGamma.ExcitonRatio + weightB * yieldsBeta.ExcitonRatio
		yields.Lindhard = weightG * yieldsGamma.Lindhard + weightB * yieldsBeta.Lindhard;
		yields.ElectricField = weightG * yieldsGamma.ElectricField + weightB * yieldsBeta.ElectricField;
		yields.DeltaT_Scint = weightG * yieldsGamma.DeltaT_Scint + weightB * yieldsBeta.DeltaT_Scint
		
		yields.PhotonYield *= E
		yields.ElectronYield *= F

		quanta = NESTcalc.GetQuanta( yields )
		quantas.append( quanta )
	
	return np.array( quantas )

# Make GetS1 usable with array/list inputs, while also simplifying the number of commands needed at runtime 
# (There are unneccessary C++ commands that get forced on us by the default bindings....)
	
def generate_S1( NESTcalc, interaction, energy_array, field, gen_quanta, xPos, yPos, zPos, driftVelocity ): 	
	S1s = []
	for i in range(len(energy_array)):
		S1 = NESTcalc.GetS1( gen_quanta[i], xPos[i], yPos[i], zPos[i], xPos[i], yPos[i], zPos[i], driftVelocity, driftVelocity, interaction, 0, field, energy_array[i], 0, False, [0], [0])
		S1s.append( S1[5] )
	return np.array( S1s )

def generate_S1_highEnergy( NESTcalc, interaction, energy_array, field, gen_quanta, xPos, yPos, zPos, driftVelocity ):
        S1s = []
        for i in range(len(energy_array)):
                S1 = NESTcalc.GetS1( gen_quanta[i], xPos[i], yPos[i], zPos[i], xPos[i], yPos[i], zPos[i], driftVelocity, driftVelocity, interaction, 0, field, energy_array[i], -1, False, [0], [0])
                S1s.append( S1[5] )
        return np.array( S1s )

# Do the same for GetS2...
def generate_S2( NESTcalc, interaction, energy_array, field, gen_quanta, xPos, yPos, zPos, driftVelocity, driftTimes ):
	g2Info = NESTcalc.CalculateG2(False)
	S2s = []
	for i in range(len(energy_array)):
		S2 = NESTcalc.GetS2( gen_quanta[i].electrons, xPos[i], yPos[i], zPos[i], xPos[i], yPos[i], zPos[i], driftTimes[i], driftVelocity, 0, field, 0, False, [0], [0], g2Info )
		S2s.append( S2[7] )
	return np.array( S2s )

#Do a hack of GetS2 to generate Single Electrons
#   Effectively, always make driftTime = 0, and extraction efficienty = 100%
#   And generate the S2 for a single electron
def generate_SE( NESTcalc, drift_field, xPos, yPos, zPos, driftVelocity ):
	g2Info = NESTcalc.CalculateG2(False)
	g2Info[1] = 1.00 # make extraction efficiency 1. This only effects the N_Extracted_Electrons calculation
	SEs = []
	for i in range(len(xPos)):
		SE = NESTcalc.GetS2( 1, xPos[i], yPos[i], zPos[i], xPos[i], yPos[i], zPos[i], 0., driftVelocity, 0, drift_field, 0, False, [0], [0], g2Info )
		SEs.append( abs( SE[6] ) ) # uncorrected SE area 
	return np.array( SEs )

#Define some functions just for ease of fitting with curve_fit(...)
def gaussian(x, mu, sigma, norm):
	return norm*np.exp( -( (x-mu)*(x-mu)/sigma/sigma )/2. )

'''def skew_gaussian(x, mu, sigma, norm, alpha):
	delta = alpha/np.sqrt(1. + alpha*alpha)
	skew_correction = 2*delta*delta/np.pi
	omega = sigma/np.sqrt( 1 - skew_correction )
	xi = mu - omega*np.sqrt(skew_correction)	
	return norm*ss.skewnorm.pdf(x, alpha, xi, omega)
'''
def skew_gaussian(x, mu, sigma, norm, alpha):
	delta = alpha/np.sqrt(1. + alpha*alpha)
	skew_correction = 2.*delta*delta/np.pi
	omega = sigma/np.sqrt(1. - skew_correction)
	xi = mu - omega*np.sqrt(skew_correction)
	y = (x - xi)/omega
	PDF = 1./(omega*np.sqrt(2.*np.pi))  *  np.exp(-0.5*y*y)  * (1. + erf( alpha * y / np.sqrt(2.) ) )
	#       Normalization Coefficient   *   Gaussian PDF     *  skewed Gaussian CDF 
	return norm * PDF


def convertMu2Xi(mu, sigma, norm, alpha):
	delta = alpha/np.sqrt(1. + alpha*alpha)
	skew_correction = 2*delta*delta/np.pi
	omega = sigma/np.sqrt( 1 - skew_correction )
	xi = mu - omega*np.sqrt(skew_correction)
	return xi, omega, alpha

def WoodsFunction(x, a, b, c, d):
    return a/(x+b)+c*x+d

def fit_Wood( x_array, y_array ):
	
	popt, pcov = curve_fit(WoodsFunction, x_array, y_array, p0=[10., 15., -1.5e-3, 2.])	
	x = np.linspace(min(x_array), max(x_array), len(x_array)*100 )
	y = WoodsFunction( x, *popt )
	return x, y, popt, np.sqrt(np.diag(pcov))

def fit_band( x_array, y_array, xMin, xMax, xStep, fitType, yType, widthType ):
	sorted_X = sorted(x_array)
	sorted_Y = [y for x,y in sorted(zip( x_array, y_array ) )]  #Sort the Y based on X	
	
	binCenters, means, mean_errs, widths, width_errs = [], [], [], [], []
	theseX, theseY = [], []
	skewFit = False
	if "skew" in fitType or "Skew" in fitType:
		print("Fitting Skewed Gaussians for band parameterization")
		skewFit = True
	else:
		if "Gauss" in fitType or "gauss" in fitType:
			print("Fitting Gaussians for band parameterization")
		else:
			print("Unrecognized fit style for band parameterization! \n Please use 'Gaussian' or 'Skew'")
			exit()
	for i in range(len(sorted_X)-1):
		#S1s are sorted, so we can just loop through, and bin
		theseX.append( sorted_X[i] )
		theseY.append( sorted_Y[i] )
		if sorted_X[i+1] > xMin+xStep:
			#we've moved out of the bin, now fit a gaussian, and move on
			histY, histX = np.histogram( theseY, bins=50, range=[0.95*min(theseY), 1.05*max(theseY)] )
			histCenters, fitTarget = [], []
			for j in range(len(histY)):
				fitTarget.append( histY[j] )
				histCenters.append( np.mean( [histX[j], histX[j+1]] ) )
			if skewFit:
				popt, pcov = curve_fit(skew_gaussian, histCenters, fitTarget, bounds=([min(theseY), 0.5*np.std(theseY), 0., -10],[max(theseY), 1.5*np.std(theseY), 10.*len(theseY), 10.]))
			else:
				popt, pcov = curve_fit( gaussian, histCenters, fitTarget, p0=[np.mean(theseY), np.std(theseY), len(theseY)] )
			perr = np.sqrt(np.diag(pcov))
		
		
			binCenters.append( xMin + xStep/2. )
			#b = xMin + xStep/2.
			#print(b)
			if yType == "mean" or yType == "Mean":
				means.append( popt[0] )
			else: 
				if skewFit == True:
					skew_params = convertMu2Xi( *popt )
					means.append( ss.skewnorm.ppf( 0.5, skew_params[2], skew_params[0], skew_params[1] ) ) #median based on point-percent function at 0.5 (inverse CDF )
				else:
					means.append( ss.norm.ppf( 0.5, popt[0], popt[1] ) )  #median based on point-percent function at 0.5 (inverse CDF )
			mean_errs.append( perr[0] )
			if "var" in widthType:
				widths.append( 1.282*popt[1] ) # 90% CL
			else:
				if skewFit == True:
					skew_params = convertMu2Xi( *popt )
					median = ss.skewnorm.ppf( 0.5, skew_params[2], skew_params[0], skew_params[1] )
					widthLo = median - ss.skewnorm.ppf( 0.10, skew_params[2], skew_params[0], skew_params[1] ) 
					widthHi =  ss.skewnorm.ppf( 0.90, skew_params[2], skew_params[0], skew_params[1] ) - median
					widths.append( np.array([widthLo, widthHi])  )
				else:
					median = ss.norm.ppf( 0.5, popt[0], popt[1] ) 
					widthLo = median - ss.norm.ppf( 0.1, popt[0], popt[1] )
					widthHi = ss.norm.ppf(0.90, popt[0], popt[1]) - median
					widths.append( np.array([widthLo, widthHi]) )

			xMin += xStep
			theseX, theseY = [], []
		if xMin >= xMax:
		        break

	return np.array(binCenters), np.array(means), np.array(mean_errs), np.array(widths), np.array(width_errs) 

def get_deviation( data1, data2 ):
	#takes two data sets, and get the average absolute deviation between them
	if ( len(data1) != len(data2) ):
		print( "Data sets are not equal lengths! %i vs. %i" % (len(data1), len(data2)) )
		return 999
	deviation = 0.
	count = 0
	for i in range(len(data1)):
		if data1[i] != 0.:
			deviation += abs(data1[i] - data2[i])/data1[i]
			count += 1
	return deviation/count
		
def calculate_ChiSq( data1, data2, error1, error2, nFreeParams ):
	chisq = 0.
	if ( len(data1) != len(data2) ):
		print( "Data sets are not equal lengths! %i vs. %i" % (len(data1), len(data2)) )
		return 999
	count = 0
	for i in range(len(data1)):
		if ( data1[i] > 0 or data2[i] > 0 ):
			chisq +=  pow( data1[i] - data2[i], 2. )/( error1[i]**2 + error2[i]**2 ) 
			count += 1

	DoF = count - nFreeParams
	return chisq/( DoF - 1 )

def calculate_SimpleChiSq( data1, data2 ):
	chisq = 0.
	if ( len(data1) != len(data2) ):
		print( "Data sets are not equal lengths! %i vs. %i" % (len(data1), len(data2)) )
		return 999
	count = 0
	for i in range(len(data1)):
		if data1[i] > 0:
			chisq +=  pow( (data1[i] - data2[i])/(data1[i]), 2. )
			count += 1

	return chisq/count


