# Make S1-S2 bands for LZ!

The purpose of this code package is to streamline how bands are made and portrayed for LZ analyses. 
The initial usage of this code allows for event generation via nestpy to produce a resultant ER or NR band, using the file `makeFlatBand.py`. As the name implies, a flat energy spectrum is used to generate events between some minimum and maximum S1. Additionally, this package is equipped with scripts that load in a .txt or a ROOT file containing S1s and S2s and produces the resultant band. 

These scripts are designed to be robust to generate any type of band desired. **The recommended band format is to fit skewed-Gaussians and reported the medians, 10%, and 90% CL for each S1 bin.** Using log10(S2) or log10(S2/S1) are both valid options. This code, however, allows the user to generate either skew bands or gaussian bands, or to report medians vs. means, or to report widths as the root-variance about the mean or as 10% 90% confidence levels. 

For each band, the code will fit the Woods Function to the resultant band means and upper/lower width lines:

        F(x) =  A/(x+B) + Cx + D

The Woods Parameters -- A, B, C, D -- will be printed to screen in order, and the Woods Fit and the fitting target will be plotted. 

---

# Get the code and setup

This code is designed to work anywhere with CVMFS access (like NERSC)
To use get the code, simply clone from this git repository:

    git clone git@gitlab.com:grischbieter/bandMaker.git

Then to compile and use, source the setup.sh script:

    source setup.sh

This will install nestpy_lz and get you the LZ_Detector.hh file for event generation. If you only wish to load events from files, and you don't care about using nestpy to generate events, you simply need to source the LzBuild setup:

    source /cvmfs/lz.opensciencegrid.org/LzBuild/latest/setup.sh

Right on! Now you can use these scripts!

---

# Usage 

As of initial release, there are two main scripts to use: `makeFlatBand.py` and `makeBandFromTxtFile.py`. For info on usage, run the scripts without any arguments:
e.g.
    python makeFlatBand.py

And the usage info will be printed to screen

#### makeFlatBand.py

This uses nestpy to generate either NR events, beta ER, gamma ER, or a beta-gamma weighted ER band. It takes 9 arguments:

         python makeFlatBand.py <interaction> <detector_condition> <band_style> <fit_style> <band_centers> <band_width> <min S1> <max S1> <nBins>
         
         -- interaction:
                 Options: 'NR', 'beta', 'gamma', 'ER'
                 ( 'ER' is a weighting of beta and gamma ER yield models )
         -- detector_conditions:
                 Options: 'SR1', 'MDC3', or a comma-delineated list of g1, g1-gas, and field, e.g. '0.11,0.10,310'
         -- band_style:
                 Options: 'logS2' or 'logS2S1'
         -- fit_style:
                 Options: 'gaussian' or 'skew'
         -- band_centers:
                 Options: 'means' or 'medians', both based on the fit
         -- band_width:
                 Options: 'root_variance' or '90-10_CLs', should be the same for Gaussian fits, but the latter will be asymmetric for Skew fits
         -- min S1:
                 The mininum S1 included in the band fitting
         -- max S1:
                 The maximum S1 included in the band fitting
         -- nBins:
                 The integer number of S1 bins between min/max S1. The number of events simulated increases with the number of nBins 
 
The script will save band information to a .txt file, while also plotting the Woods Function fit and printing the Woods Parameters to screen.

Example: 

     python makeFlatBand.py  beta 0.1184,0.0947,310 logS2 skew medians 90-10_Cls 1.5 100.5 99 

#### makeBandFromTxtFile.py

This script takes in either the relative or absolute path to a .txt file containing S1,S2 events, e.g.:

    S1c [phd]       S2c [phd] 
    45.6111         18593.5
    231.426         70205.0
    28.3305         20530.6
    76.6218         44934.8
    208.499         96695.0
    68.9765         28451.2
    139.207         69156.3
    156.387         95676.6
    136.987         67405.9
    163.906         72940.7
    226.768         82380.0
    181.733         81006.9
    170.773         87854.6
    86.1403         42219.0 


Column headers aren't needed, but they're okay to keep in the file when band making. Included a text file path, this code takes 8 arguments:


    python makeBandFromTxtFile.py <path to S1,S2 txt file> <band_style> <fit_style> <band_centers> <band_width> <min S1> <max S1> <nBins>
    
         -- txt file path:
                 Can be relative or absolute path
                 File must contain S1s in the first column, and S2s in the second column (Column Headers in 1st Row are OK)
         -- band_style:
                 Options: 'logS2' or 'logS2S1'
         -- fit_style:
                 Options: 'gaussian' or 'skew'
         -- band_centers:
                 Options: 'means' or 'medians', both based on the fit
         -- band_width:
                 Options: 'root_variance' or '90-10_CLs', should be the same for Gaussian fits, but the latter will be asymmetric for Skew fits
         -- min S1:
                 The mininum S1 included in the band fitting
         -- max S1:
                 The maximum S1 included in the band fitting
         -- nBins:
                 The integer number of S1 bins between min/max S1. The number of events simulated increases with the number of nBins 

Band info will be saved to an output .txt file, Woods parameters are printed, and the fit is plotted. 

Example:

     python makeBandFromTxtFile.py myFile.txt logS2  gaussian  mean  root-variance 1.5 300.5 299

#### makeBandFromROOTFile.py

This script operates similarly to `makeBandFromTxtFile.py`, however, it loads in a ROOT file. 
The ROOT file must have the default LZAP RQ structure for S1s and S2s. Specifically, it looks for a `Scatters` TTree, and the branches `ss.correctedS1Area_phd` and `ss.correctedS2Area_phd`. 
This script and function takes 8 input arguments:

    python makeBandFromROOTFile.py <path to LZAP RQ ROOT file> <band_style> <fit_style> <band_centers> <band_width> <min S1> <max S1> <nBins>
    
         -- ROOT file path:
                 Can be relative or absolute path
                 File must contain a 'Scatters' TTree with the branches 'correctedS1Area_phd' and 'correctedS2Area_phd'
         -- band_style:
                 Options: 'logS2' or 'logS2S1'
         -- fit_style:
                 Options: 'gaussian' or 'skew'
         -- band_centers:
                 Options: 'means' or 'medians', both based on the fit
         -- band_width:
                 Options: 'root_variance' or '90-10_CLs', should be the same for Gaussian fits, but the latter will be asymmetric for Skew fits
         -- min S1:
                 The mininum S1 included in the band fitting
         -- max S1:
                 The maximum S1 included in the band fitting
         -- nBins:
                 The integer number of S1 bins between min/max S1. The number of events simulated increases with the number of nBins



Band info will be saved to an output .txt file, Woods parameters are printed, and the fit is plotted.

Example:

     python makeBandFromROOTFile.py myLZAPFile.ROOT logS2S1  gaussian  median  90-10_CLs 1.5 100.5 33
---

# Contact

Reach out to Greg R. for questions. 

Email: grischbieter@albany.edu
Slack: @GregR 
