#!/bin/bash

#source the LZ Build from CVMFS

source /cvmfs/lz.opensciencegrid.org/LzBuild/latest/setup.sh


# get the nestpy_lz project maintained by GR

git clone git@gitlab.com:grischbieter/nestpy_lz.git
cd nestpy_lz

source initial_setup.sh

cd ..

echo "Done!"
