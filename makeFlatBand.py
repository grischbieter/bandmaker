import nestpy
import matplotlib.pyplot as plt
import scipy.stats as ss
from scipy.optimize import curve_fit
from matplotlib.colors import LogNorm
import numpy as np
import sys
from core_functions import *


def makeFlatBand(interaction, detector_conditions, bandStyle, fitStyle, bandCenterType, widthType, minS1, maxS1, nBins):
	'''
	'''
	# Create the detector and nestcalc objects !!
	
	detector = nestpy.LZ_Detector()
	nc = nestpy.NESTcalc( detector )
	output_string = "flatBand_"
	if detector_conditions == "SR1":
		output_string += "SR1_"
		g1 = detector.get_g1()
		g2Info = nc.CalculateG2(False)
		g2 = g2Info[3]
		field = 310.
		print("Using SR1 Projected Detector...  g1=%.4f, g2=%.2f" % (g1, g2) )
	else:
		if detector_conditions == "MDC3":
			output_string += "MDC3_"
			detector.set_g1(0.150)
			detector.set_g1_gas( 0.13765 )
			detector.set_eLife_us(944.56)
			field = 310.
			g1 = detector.get_g1()
			g2Info = nc.CalculateG2(False)
			g2 = g2Info[3]
			print("Using MDC3 Detector... g1=%.4f, g2=%.2f" % (g1, g2) )
		else:
			try:
				detector_params = detector_conditions.split(",")
				detector.set_g1(float(detector_params[0]))
				detector.set_g1_gas(float(detector_params[1]))
				output_string += "Custom-"+detector_conditions+"_"
				field = float(detector_params[2])
				g1 = detector.get_g1()
				g2Info = nc.CalculateG2(False)
				g2 = g2Info[3]
				print("Using Custom Detector... g1=%.4f, g2=%.2f" % (g1, g2) )
			except:
				print( "Please use a valid detector condition setting!" )
			
	
	
			
	'''
	Next, we want to create arrays of energies and positions
	'''
	eMin = minS1/g1/50.*0.4
	eMax = maxS1/g1/25.
	if eMin < 1.:
		eMin = 0.
	nEvents = 5000*int(nBins)
	
	# Example: flat beta spectrum ( like our Rn calibration, sort of )
	# but could easily load any other energy input
	if interaction == 'beta':
		output_string += 'beta_'
		interactionType = nestpy.INTERACTION_TYPE(8) # beta ER
	if interaction == 'gamma':
		output_string += 'gamma_'
		interactionType = nestpy.INTERACTION_TYPE(7) # gamma ER 
	if interaction == 'NR':
		output_string += 'NR_'
		interactionType = nestpy.INTERACTION_TYPE(0) # nuclear recoil
		eMin = minS1
		eMax = 0.6*maxS1
		if eMin < 2.:
			eMin = 0.
	

	energies = np.random.uniform(eMin, eMax, nEvents)
	
	random_R2 = np.random.uniform( 0., 698.*698., nEvents )
	random_phi = np.random.uniform( 0., 2.*np.pi, nEvents )
	random_X = np.sqrt( random_R2 ) * np.cos( random_phi )
	random_Y = np.sqrt( random_R2 ) * np.sin( random_phi )
	random_driftTimes = np.random.uniform( 100., 700., nEvents )
	
	density = nc.SetDensity( detector.get_T_Kelvin(), detector.get_p_bar() )
	
	driftVelocity = nc.SetDriftVelocity(detector.get_T_Kelvin(), density, field )
	random_Z = detector.get_TopDrift() - random_driftTimes*driftVelocity
	if interaction == 'ER':
		output_string += 'ER_'
		quantas = generate_quanta_ERweighted( nc, energies, field )	
		interactionType = nestpy.INTERACTION_TYPE(8) # beta ER
	else:
		quantas = generate_quanta( nc, interactionType, energies, field )

	S1s = generate_S1( nc, interactionType, energies, field, quantas, random_X, random_Y, random_Z, driftVelocity )
	S2s = generate_S2( nc, interactionType, energies, field, quantas, random_X, random_Y, random_Z, driftVelocity, random_driftTimes )
	#print(min(S1s), max(S1s))
	#print(eMin, eMax)
	#plt.plot( S1s, np.log10(S2s), 'c o', ms=2, alpha=0.5 )
	roi_cut = (S1s > minS1) & (S1s <= maxS1) & (S2s > 0.) 

	'''
	I've made a function that fits gaussian bands to an X vs. Y set
	and a function that returns the average absolute deviation between two arrays ( see definitions below main() )
	'''
	if bandStyle == 'logS2':
		yValues = np.log10(S2s[roi_cut])
		output_string += 'logS2_'
	else:
		yValues = np.log10( S2s[roi_cut]/S1s[roi_cut] )
		output_string += 'logS2S1_'
	output_string += fitStyle+"_" 
	binCenters, means, mean_errs, widths, width_errs = fit_band( S1s[roi_cut], yValues, minS1, maxS1, (maxS1 - minS1)/nBins, fitStyle, bandCenterType, widthType )	
	return binCenters, means, mean_errs, widths, width_errs, output_string


def main():
		
		try:
			interaction, detector_conditions = sys.argv[1], sys.argv[2]
			bandStyle, fitStyle, bandCenterType, widthType = sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6]
			minS1, maxS1, nBins = float(sys.argv[7]), float(sys.argv[8]), float(sys.argv[9])
		except:
			print( "This script and function takes 9 input arguments: \n")
			print( "Example Usage: python makeFlatBand.py <interaction> <detector_condition> <band_style> <fit_style> <band_centers> <band_width> <min S1> <max S1> <nBins> \n" )
			print( "\t -- interaction:")
			print( "\t \t Options: 'NR', 'beta', 'gamma', 'ER'" )
			print( "\t \t ( 'ER' is a weighting of beta and gamma ER yield models )" )
			
			print( "\t -- detector_conditions:" )
			print( "\t \t Options: 'SR1', 'MDC3', or a comma-delineated list of g1, g1-gas, and field, e.g. '0.11,0.10,310'")
			
			print( "\t -- band_style: ")
			print ("\t \t Options: 'logS2' or 'logS2S1'")
			
			print( "\t -- fit_style: " )
			print( "\t \t Options: 'gaussian' or 'skew'" )
			
			print( "\t -- band_centers: ")
			print( "\t \t Options: 'means' or 'medians', both based on the fit" )
			
			print( "\t -- band_width: ")
			print( "\t \t Options: 'root_variance' or '90-10_CLs', should be the same for Gaussian fits, but the latter will be asymmetric for Skew fits" )
			
			print( "\t -- min S1: ")
			print( "\t \t The mininum S1 included in the band fitting" )
			
			print( "\t -- max S1: ")
			print( "\t \t The maximum S1 included in the band fitting" )
			
			print( "\t -- nBins: ")
			print( "\t \t The integer number of S1 bins between min/max S1. The number of events simulated increases with the number of nBins" )
			
			return 1
		binCenters, means, mean_errs, widths, width_errs, output_string = makeFlatBand(interaction, detector_conditions, bandStyle, fitStyle, bandCenterType, widthType, minS1, maxS1, nBins)
		print("Saving band info to %s" % output_string+"band.txt")
		outputFile = open(output_string+"band.txt", 'w')
		outputFile.write('S1 Center \t Band Mean \t Band Widths\n')
		for i in range(len(binCenters)):
			#print( binCenters[i], means[i], widths[i] )
			outputFile.write(str(binCenters[i])+'\t'+str(means[i])+'\t'+str(widths[i])+'\n')
		outputFile.close()
		if "var" in widthType:
			plt.plot( binCenters, means, 'k', label='Resultant Band' )
			plt.plot( binCenters, np.array(means)+np.array(widths), 'k--')
			plt.plot( binCenters, np.array(means)-np.array(widths), 'k--')
			plt.xlabel(r'S1$_c$ [phd]', fontsize=18, family='serif')
			fitX_mean, fitY_mean, meanParams, meanParamErrs = fit_Wood( binCenters, means )
			plt.plot( fitX_mean, fitY_mean, 'r', label='Woods Function Fit' )
			print( 'The Woods Function Fit Parameters for the Band Mean are: ', meanParams )
			fitX_lo, fitY_lo, loParams, loParamErrs = fit_Wood( binCenters, np.array(means)-np.array(widths) )
			plt.plot( fitX_lo, fitY_lo, 'r--' )
			print( 'The Woods Function Fit Parameters for the -1 Sigma Line are: ', loParams )
			fitX_hi, fitY_hi, hiParams, hiParamErrs = fit_Wood(binCenters, np.array(means)+np.array(widths) )
			plt.plot( fitX_hi, fitY_hi, 'r--' )
			print( 'The Woods Function Fit Parameters for the +1 Sigma Line are: ', hiParams )
		else:
			plt.plot( binCenters, means, 'k', label='Resultant Band' )
			plt.plot( binCenters, np.array(means)+np.array(widths[:,1]), 'k--')
			plt.plot( binCenters, np.array(means)-np.array(widths[:,0]), 'k--')
			plt.xlabel(r'S1$_c$ [phd]', fontsize=18, family='serif')
			fitX_mean, fitY_mean, meanParams, meanParamErrs = fit_Wood( binCenters, means )
			plt.plot( fitX_mean, fitY_mean, 'r', label='Woods Function Fit' )
			print( 'The Woods Function Fit Parameters for the Band Mean are: ', meanParams )
			fitX_lo, fitY_lo, loParams, loParamErrs = fit_Wood( binCenters, np.array(means)-np.array(widths[:,0]) )
			plt.plot( fitX_lo, fitY_lo, 'r--' )
			print( 'The Woods Function Fit Parameters for the 10% CL Line are: ', loParams )
			fitX_hi, fitY_hi, hiParams, hiParamErrs = fit_Wood(binCenters, np.array(means)+np.array(widths[:,1]) )
			plt.plot( fitX_hi, fitY_hi, 'r--' )
			print( 'The Woods Function Fit Parameters for the 90% CL Line are: ', hiParams )
		if bandStyle == 'logS2':
			plt.ylabel(r'log$_{10}$( S2$_c$ [phd] )', fontsize=18, family='serif')
		else:
			plt.ylabel(r'log$_{10}$( S2$_c$/S1$_c$ )', fontsize=18, family='serif')
		plt.legend( loc='lower right' )
		plt.tight_layout()
		plt.show()
		return 0
		

main()















