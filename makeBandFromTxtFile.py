import matplotlib.pyplot as plt
import scipy.stats as ss
from scipy.optimize import curve_fit
from matplotlib.colors import LogNorm
import numpy as np
import sys
from core_functions import *


def makeBandFromTxtFile(txtFileName, bandStyle, fitStyle, bandCenterType, widthType, minS1, maxS1, nBins):

	try:
		textFile = np.loadtxt(txtFileName)
	except:
		#try again, skipping column headers
		textFile = np.loadtxt(txtFileName, skiprows=1)

	S1s = textFile[:,0]
	S2s = textFile[:,1]

	roi_cut = (S1s > minS1) & (S1s <= maxS1) & (S2s > 0.) 
	output_string = ((txtFileName.split('.txt'))[0]).split('/')[-1] + '_'
	'''
	I've made a function that fits gaussian bands to an X vs. Y set
	and a function that returns the average absolute deviation between two arrays ( see definitions below main() )
	'''
	if bandStyle == 'logS2':
		yValues = np.log10(S2s[roi_cut])
		output_string += 'logS2_'
	else:
		yValues = np.log10( S2s[roi_cut]/S1s[roi_cut] )
		output_string += 'logS2S1_'
	output_string += fitStyle+"_" 
	binCenters, means, mean_errs, widths, width_errs = fit_band( S1s[roi_cut], yValues, minS1, maxS1, (maxS1 - minS1)/nBins, fitStyle, bandCenterType, widthType )	
	return binCenters, means, mean_errs, widths, width_errs, output_string


def main():
		
		try:
			textFileName = sys.argv[1]
			bandStyle, fitStyle, bandCenterType, widthType = sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]
			minS1, maxS1, nBins = float(sys.argv[6]), float(sys.argv[7]), float(sys.argv[8])
		except:
			print( "This script and function takes 8 input arguments: \n")
			print( "Example Usage: python makeBandFromTxtFile.py <path to S1,S2 txt file> <band_style> <fit_style> <band_centers> <band_width> <min S1> <max S1> <nBins> \n" )
			print( "\t -- txt file path:")
			print( "\t \t Can be relative or absolute path" )
			print( "\t \t File must contain S1s in the first column, and S2s in the second column (Column Headers in 1st Row are OK)" )
			
			print( "\t -- band_style: ")
			print ("\t \t Options: 'logS2' or 'logS2S1'")
			
			print( "\t -- fit_style: " )
			print( "\t \t Options: 'gaussian' or 'skew'" )
			
			print( "\t -- band_centers: ")
			print( "\t \t Options: 'means' or 'medians', both based on the fit" )
			
			print( "\t -- band_width: ")
			print( "\t \t Options: 'root_variance' or '90-10_CLs', should be the same for Gaussian fits, but the latter will be asymmetric for Skew fits" )
			
			print( "\t -- min S1: ")
			print( "\t \t The mininum S1 included in the band fitting" )
			
			print( "\t -- max S1: ")
			print( "\t \t The maximum S1 included in the band fitting" )
			
			print( "\t -- nBins: ")
			print( "\t \t The integer number of S1 bins between min/max S1. The number of events simulated increases with the number of nBins" )
			
			return 1
		binCenters, means, mean_errs, widths, width_errs, output_string = makeBandFromTxtFile(textFileName, bandStyle, fitStyle, bandCenterType, widthType, minS1, maxS1, nBins)
		print("Saving band info to %s" % output_string+"band.txt")
		outputFile = open(output_string+"band.txt", 'w')
		outputFile.write('S1 Center \t Band Mean \t Band Widths\n')
		for i in range(len(binCenters)):
			#print( binCenters[i], means[i], widths[i] )
			outputFile.write(str(binCenters[i])+'\t'+str(means[i])+'\t'+str(widths[i])+'\n')
		outputFile.close()
		if "var" in widthType:
			plt.plot( binCenters, means, 'k', label='Resultant Band' )
			plt.plot( binCenters, np.array(means)+np.array(widths), 'k--')
			plt.plot( binCenters, np.array(means)-np.array(widths), 'k--')
			plt.xlabel(r'S1$_c$ [phd]', fontsize=18, family='serif')
			fitX_mean, fitY_mean, meanParams, meanParamErrs = fit_Wood( binCenters, means )
			plt.plot( fitX_mean, fitY_mean, 'r', label='Woods Function Fit' )
			print( 'The Woods Function Fit Parameters for the Band Mean are: ', meanParams )
			fitX_lo, fitY_lo, loParams, loParamErrs = fit_Wood( binCenters, np.array(means)-np.array(widths) )
			plt.plot( fitX_lo, fitY_lo, 'r--' )
			print( 'The Woods Function Fit Parameters for the -1 Sigma Line are: ', loParams )
			fitX_hi, fitY_hi, hiParams, hiParamErrs = fit_Wood(binCenters, np.array(means)+np.array(widths) )
			plt.plot( fitX_hi, fitY_hi, 'r--' )
			print( 'The Woods Function Fit Parameters for the +1 Sigma Line are: ', hiParams )
		else:
			plt.plot( binCenters, means, 'k', label='Resultant Band' )
			plt.plot( binCenters, np.array(means)+np.array(widths[:,1]), 'k--')
			plt.plot( binCenters, np.array(means)-np.array(widths[:,0]), 'k--')
			plt.xlabel(r'S1$_c$ [phd]', fontsize=18, family='serif')
			fitX_mean, fitY_mean, meanParams, meanParamErrs = fit_Wood( binCenters, means )
			plt.plot( fitX_mean, fitY_mean, 'r', label='Woods Function Fit' )
			print( 'The Woods Function Fit Parameters for the Band Mean are: ', meanParams )
			fitX_lo, fitY_lo, loParams, loParamErrs = fit_Wood( binCenters, np.array(means)-np.array(widths[:,0]) )
			plt.plot( fitX_lo, fitY_lo, 'r--' )
			print( 'The Woods Function Fit Parameters for the 10% CL Line are: ', loParams )
			fitX_hi, fitY_hi, hiParams, hiParamErrs = fit_Wood(binCenters, np.array(means)+np.array(widths[:,1]) )
			plt.plot( fitX_hi, fitY_hi, 'r--' )
			print( 'The Woods Function Fit Parameters for the 90% CL Line are: ', hiParams )
		if bandStyle == 'logS2':
			plt.ylabel(r'log$_{10}$( S2$_c$ [phd] )', fontsize=18, family='serif')
		else:
			plt.ylabel(r'log$_{10}$( S2$_c$/S1$_c$ )', fontsize=18, family='serif')
		plt.legend( loc='lower right' )
		plt.tight_layout()
		plt.show()
		return 0
		

main()















